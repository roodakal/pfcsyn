﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models.Domain;

namespace WebApplication1.DataAccess.Repositories
{
    public class MessagesRepository : IMessagesRepository
    {
        private readonly ApplicationDbContext _context;

        public MessagesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddMessage(Message m)
        {
            _context.Messages.Add(m);
            _context.SaveChanges();
        }

        public Message GetMessage(Guid id)
        {
            return _context.Messages.SingleOrDefault(x => x.MessageId == id);
        }

        public IQueryable<Message> GetMessages()
        {
            return _context.Messages;
        }
    }
}
