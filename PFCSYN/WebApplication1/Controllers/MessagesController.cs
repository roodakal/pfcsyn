﻿using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.DataAccess.Interfaces;
using WebApplication1.Models.Domain;

namespace WebApplication1.Controllers
{
    public class MessagesController : Controller
    {
        private readonly IMessagesRepository _messagesRepo;
        private readonly IConfiguration _config;
        private readonly IPubSubRepository _pubsubRepo;

        public MessagesController(IMessagesRepository messagesRepo, IConfiguration config, IPubSubRepository pubsubRepo)
        {
            _config = config;
            _messagesRepo = messagesRepo;
            _pubsubRepo = pubsubRepo;
        }
        public IActionResult Index()
        {
            var list = _messagesRepo.GetMessages();
            return View(list);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Create(IFormFile atch, Message m)
        {
            string bucketName = _config.GetSection("AppSettings").GetSection("BucketName").Value;
            string uniqueFilename = Guid.NewGuid() + System.IO.Path.GetExtension(atch.FileName);

            var storage = StorageClient.Create();

            using (var myStream = atch.OpenReadStream())
            {
                storage.UploadObject(bucketName, uniqueFilename, null, myStream);
            }

            m.Content = $"https://storage.googleapis.com/{bucketName}/{uniqueFilename}";

            _messagesRepo.AddMessage(m);

            //publishmessage
            string emailRecipient = HttpContext.User.Identity.Name; //the email of the current logged user
            _pubsubRepo.PublishMessage(m, emailRecipient);

            return RedirectToAction("Index");           
        }

        public IActionResult Fetch()
        {
            string msgSerialized = _pubsubRepo.PullMessage();
            if (msgSerialized == string.Empty) return Content("No message read");
            dynamic myDeserializedData = JsonConvert.DeserializeObject(msgSerialized);
            string email = myDeserializedData.Email;
            string messageText = myDeserializedData.Message.Messagetxt;
            string contentUrl = myDeserializedData.Message.Content;

            /*email = User.Identity.Name;
            messageText = */


            //return View();
            return Content("Message received");
        }
    }
}
